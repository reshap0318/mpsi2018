<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@beranda')->name('beranda');
Route::get('caripeta/{id}', 'HomeController@caripeta');
Route::get('layer','HomeController@layerpeta');
Route::get('caritype','HomeController@caritype');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('ruanganajax', 'HomeController@ruangan')->name('ruangan.ajax');
Route::get('detail-peta', 'HomeController@detail')->name('detail');

Route::resource('gedungs', 'gedungController');
Route::get('gedunggeom/{id}', 'gedungController@getMapgedung')->name('gedunggeom');
Route::get('tengahgedunggeom/{id}', 'gedungController@getCentergedung')->name('tengahgedunggeom');

Route::resource('ruangans', 'ruanganController');
Route::get('ruangangeom/{id}', 'ruanganController@getMapruangan')->name('ruangangeom');
Route::get('tengahruangangeom/{id}', 'ruanganController@getCenterruangan')->name('tengahruangangeom');

Route::resource('fasilitas', 'fasilitasController');

Route::resource('ruanganFasilitas', 'ruanganFasilitasController');
