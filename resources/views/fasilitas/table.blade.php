<table class="table table-responsive" id="fasilitas-table">
    <thead>
        <tr>
            <th>Nama</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($fasilitas as $fasilitas)
        <tr>
            <td>{!! $fasilitas->nama !!}</td>
            <td>
                {!! Form::open(['route' => ['fasilitas.destroy', $fasilitas->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('fasilitas.show', [$fasilitas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('fasilitas.edit', [$fasilitas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>