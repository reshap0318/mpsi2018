<?php
  include "awalan/koneksi.php";
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- CSS Saya -->
    <link rel="stylesheet" href="{{asset('awalan/style.css')}}">

    <!-- Font Viga -->
    <link href="https://fonts.googleapis.com/css?family=Viga" rel="stylesheet">

    <!-- Font dan Lainnya -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <title>Peta Unand</title>
    <script type="text/javascript" src="{{asset('bb/webclient/js/jquery.js')}}" ></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=visualization"></script>

    <script src="{{asset('bb/webclient/js/jquery-1.9.1.min.js')}}"></script>
<script type="text/javascript">
  $(function(){
		$(document).on('change','#gedung',function()
			{
				var tipe=document.getElementById("gedung").options[document.getElementById("gedung").selectedIndex].value;
				$('#ruangan').html("");
					$.ajax({
				 		url: 'ruanganajax?gedungid='+tipe+'', data: "", dataType: 'json', success: function(rows)
				  			{
                  $('#ruangan').append('<option value="">--Pilihan Ruangan--</option>');
				  				for (var i in rows)
									{
										var row = rows[i];
										var idjenis=row.id;
										var jenis=row.nama;
										$('#ruangan').append('<option value="'+idjenis+'">'+jenis+'</option>');
					 				}
							}
						});
			});
  });
</script>

<script type="text/javascript">

  var infowindow;
  var geomarker;
  var markerarray = [];
  var map;
  var objek;
  var directionsService;
  var directionDisplay;
  var usegeolocation;
  var server="<?php echo Request::root(); ?>";
  var markerarraygeo=[];
  var circlearray=[];
  var layernya;

  function initialize() {
     geolocation();
     basemap();
  }
  function basemap() {
  		google.maps.visualRefresh = true;
      map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: new google.maps.LatLng(-0.914813, 100.458801),
          mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      loadLayer();
  };

  function loadLayer(){
      layernya = new google.maps.Data();
      layernya.loadGeoJson(server+'/layer');
      layernya.setMap(map);
  }
      function loaddata(results){
        	if(results.features === null)
               {
                   alert("Data yang dicari tidak ada");
               }
               else
               {
                for (var i = 0; i < results.features.length; i++) {
                    var data = results.features[i];
                    var coords = data.geometry.coordinates;
                    var id = data.properties.id;
                    var nama = data.properties.nama;
                    var alamat= data.properties.alamat;
                    var deskripsi=data.properties.deskripsi;
                    var titiktengah = data.properties.center
                    var latitude = titiktengah.lat;
                    var longitude = titiktengah.lng;
                    var latLng = new google.maps.LatLng(latitude,longitude);
        	          var gambar=data.properties.image; //menmpilkan informasi pada pencarian
                    //$('.listdaftar').append('<p><b>'+nama+'</b></p><p>Travel Mode :<select id="travelmode"><option value="DRIVING">Driving</option><option value="WALKING">Walking</option><option value="BICYCLING">Bicycling</option><option value="TRANSIT">Transit</option></select></p><button id="'+i+'" class="buttongetdirection">Get Direction</button><br>');
                    $('.listdaftar').append('<div class="panel-heading"><h2>'+nama+'</h2></div><div class="panel-body"><label for="gedung">Travel Mode</label><select id="travelmode" class="form-control dynamic" required><option value="DRIVING">DRIVING</option><option value="WALKING">WALKING</option><option value="BICYCLING">BICYCLING</option><option value="TRANSIT">TRANSIT</option></select><div class="text-center"><a class="btn btn-primary btn-hitung buttongetdirection" id="'+i+'">Get Direction</a></div></div>');
                    //$('#berita').append('<div id="berita'+id+'" style="width:600px;display:none;text-align:justify;"><h2><b>'+nama+'</b></h2></div>');

                    var iconBase = 'http://maps.google.com/mapfiles/kml/pal2/icon49.png';
                    var marker = new google.maps.Marker({
                      position: latLng,
                      map: map,
                      animation: google.maps.Animation.DROP,
                      icon: iconBase,
                      title: nama
                      //shadow: iconBase + 'schools_maps.shadow.png'
                    });
        	           markerarray.push(marker); //menmpilkan informasi pada marking
                     var isiinfo="<div style='width:400px; min-height:100px;'><b><h2><center>"+nama+"</center></h2></b></div>";
                     createInfoWindow(marker, isiinfo);
               }
        	}

	$('.buttongetdirection').click(function(){
		  $("#daftar").prepend('<div  class="panel panel-info"  id="paneldirection"></div>');
    		  var k=this.id;
                var titikawal=geomarker.getPosition();
                var titikakhir=markerarray[k].getPosition();
                calcRoute(titikawal,titikakhir);
                clearmarker();
    		  $('.listdaftar').html('');
	      });
     }

     var infowindow = new google.maps.InfoWindow();
     function createInfoWindow(marker, isiinfo) {
        google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(isiinfo);
        infowindow.open(map, this);
        });
     }
    google.maps.event.addDomListener(window, 'load', initialize);

  function calcRoute(start, end) {
     var travelmode = document.getElementById("travelmode").options[document.getElementById("travelmode").selectedIndex].value;
     directionsService = new google.maps.DirectionsService();
        var request = {
         origin:start,
         destination:end,
         travelMode: google.maps.TravelMode[travelmode],
         unitSystem: google.maps.UnitSystem.METRIC,
         provideRouteAlternatives: true
     };
     directionsService.route(request, function(response, status) {
       if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        }
     });
     directionsDisplay = new google.maps.DirectionsRenderer({draggable: false});
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById('paneldirection'));
   }

   function clearmarkergeo(){
         for (var i = 0; i < markerarraygeo.length; i++) {
          markerarraygeo[i].setMap(null);
         }
         markerarraygeo=[];
     }

     function clearmarker(){
         for (var i = 0; i < markerarray.length; i++) {
          markerarray[i].setMap(null);
         }
         markerarray=[];
     }

     function clearroute(){
         directionsDisplay.setMap(null);
     }


   function geolocation(){
     navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError);
     }

     function geolocationSuccess(posisi){
     var pos=new google.maps.LatLng(posisi.coords.latitude,posisi.coords.longitude);
     geomarker = new google.maps.Marker({
          map: map,
          position: pos,
          animation: google.maps.Animation.DROP
    });

      map.panTo(pos);
        infowindow = new google.maps.InfoWindow();
        infowindow.setContent('Your Position');
        infowindow.open(map, geomarker);
      usegeolocation=true;
     }

     function geolocationError(err){
         usegeolocation=false;

     }
  </script>
  <script type="text/javascript">
	$(function(){
		$('#cari').click(function(){
			$('#paneldirection').html('');
			clearmarker();
			$(".listdaftar").html("");
			var hasil = document.getElementById("ruangan").value;
			//var jenis = document.getElementById("jenis").value;
			var script1 = document.createElement('script');
              script1.src = server+'/caripeta/'+hasil+'';
			document.getElementsByTagName('head')[0].appendChild(script1);
			clearroute();

			});
	});

	</script>
  <script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3UDFwI28UYpMQQqpV7YxlSBShk-7fGCc&callback=initMap">
  </script>




  </head>
  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
      <a class="navbar-brand" href="#">PETAUNAND</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav ml-auto">
          </div>
      </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->

    <!-- Jumbotron -->
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="display-4">Aplikasi <span>Peta Unand</span> Terpadu</h1>
          <a href="#detail" class="btn btn-primary tombol">Lebih Lanjut</a>
      </div>
    </div>
    <!-- Akhir Jumbotron -->

    <!-- Container -->
    <div class="container">
      <!-- Info Panel -->
      <div class="row justify-content-center">
        <div class="col-10 info-panel">
          <div class="row">
            <div class="col-lg">
              <img src="{{asset('awalan/img/kaca-pembesar.png')}}" alt="" class="float-left">
              <h4>Pencarian</h4>
              <p>Pencarian data lengkap untuk fasilitas yang ada di Universitas Andalas</p>
            </div>
            <div class="col-lg">
              <img src="{{asset('awalan/img/security.png')}}" alt="" class="float-left">
              <h4>Informasi</h4>
              <p>Informasi lengkap terkait dengan penggunaan fasilitas yang ada di Universitas Andalas</p>
            </div>
            <div class="col-lg">
              <img src="{{asset('awalan/img/map-localization.png')}}" alt="" class="float-left">
              <h4>Navigasi</h4>
              <p>Petunjuk untuk menuju ke titik fasilitas yang ada di Universitas Andalas</p>
            </div>
          </div>
        </div>
      </div>
      <!-- Akhir info panel -->

      <!-- Konten -->
      <div class="row workingspace justify-content-center" id="detail">
        <div class="col-md-6">
          <div class="panel panel-info">
              <div class="panel-heading">
                      <h2><i class="fa fa-car"></i> Hitung Jarak Dan Waktu Tempuh</h2>
              </div>
              <div class="panel-body">
                  <form class="form" action="" method="post">
                      <div class="form-group">
                          <label for="gedung">Pilih Gedung</label>
                          <select name="gedung" class="form-control dynamic" data-dependent='ruangans' id='gedung' required>
                              <option value="">--Pilihan Gedung--</option>
                                @foreach($gedungs as $gedung)
                                  <option value="{{$gedung->id}}">{{$gedung->nama}}</option>
                                @endforeach
                          </select>
                      </div>

                      <div class="form-group">
                          <label for="ruangan">Pilih Ruangan</label>
                          <select id="ruangan" name="ruangan" class="form-control dynamic" required>
                              <option value="">--Pilihan Ruangan--</option>
                                  <!-- tambah opsi disini  -->
                          </select>

                      </div>
                  </form>
                  <div class="text-center">
                  <a class="btn btn-primary btn-hitung" id='cari'>Hitung</a>
                  </div>
              </div>
          </div>
          <div id='daftar'>
            <div id="hasildata" class="panel panel-info listdaftar"></div>
          </div>
        </div>

        <div class="col-md-6">
          <div style="height:500px;" id="map" class="map">
          </div>
        </div>
      </div>

      <!-- Akhir Konten -->

      <!-- Testimoni -->
      <section class="testimonial">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <h5>Aplikasi ini diharapkan dapat membantu mahasiswa atau pengunjung dalam menemukan fasilitas yang ada di kampus Universitas Andalas ini</h5>
          </div>
        </div>

        <div class="row justify-content-center">
          <div class="col-6 justify-content-center d-flex">
            <figure class="figure">
              <img src="{{asset('awalan/img/hafiz.jpg')}}" class="figure-img img-fluid rounded-circle" alt="testi1">
            </figure>
            <figure class="figure">
              <img src="{{asset('awalan/img/hafiz.jpg')}}" class="figure-img img-fluid rounded-circle utama" alt="testi1">
                <figcaption class="figure-caption">
                  <h5>Hafiz</h5>
                  <p>Mahasiswa</p>
                </figcaption>
            </figure>
            <figure class="figure">
              <img src="{{asset('awalan/img/hafiz.jpg')}}" class="figure-img img-fluid rounded-circle" alt="testi1">
            </figure>
          </div>
        </div>
      </section>
      <!-- Akhir Testimoni -->
    </div>
    <!-- Akhir Container -->

    <!-- Footer -->
    <div class="row">
      <div class="col text-center footer">
        <p>2018 &copy All Rights Reserved by Sistem Informasi Universitas Andalas</p>
      </div>
    </div>



  </body>
</html>
