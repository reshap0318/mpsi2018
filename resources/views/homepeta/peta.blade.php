<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=visualization"></script>

<script src="{{asset('bb/webclient/js/jquery-1.9.1.min.js')}}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('bb/webclient/css/easyui.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('bb/webclient/css/icon.css')}}">

<script type="text/javascript" src="{{asset('bb/webclient/js/jquery.easyui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bb/webclient/js/jquery.edatagrid.js')}}"></script>

<!--Fancybox-->
<script type="text/javascript" src="{{asset('bb/webclient/fancy/jquery.mousewheel-3.0.6.pack.js')}}"></script> <!-- Sertakan JQuery mousewheel untuk image gallery!-->
<script type="text/javascript" src="{{asset('bb/webclient/fancy/source/jquery.fancybox.js?v=2.1.5')}}"></script> <!-- Sertakan JQuery fancybox dan cssnya-->
<link rel="stylesheet" type="text/css" href="{{asset('bb/webclient/fancy/source/jquery.fancybox.css?v=2.1.5')}}" media="screen" />
<script type="text/javascript">
  $(function(){
	  $('.fancybox').fancybox();
	  $.ajax({
		url: "{{url('caritype')}}", data: "", dataType: 'json', success: function(rows)
			{
				for (var i in rows)
					{
						var row = rows[i];
						$('#selecttipe').append('<option value="'+row['id']+'">'+row['nama']+'</option>');
		 			}
		 	}
		 });
		$(document).on('change','#selecttipe',function()
			{
				var tipe=document.getElementById("selecttipe").options[document.getElementById("selecttipe").selectedIndex].value;
				$('#selectjenis').html("");
					$.ajax({
				 		url: 'ruanganajax?gedungid='+tipe+'', data: "", dataType: 'json', success: function(rows)
				  			{
				  				for (var i in rows)
									{
										var row = rows[i];
										var idjenis=row.id;
										var jenis=row.nama;
										$('#selectjenis').append('<option value="'+idjenis+'">'+jenis+'</option>');
					 				}
							}
						});
			});
  });
</script>

<script type="text/javascript">

	var infowindow;
	  var geomarker;
	  var markerarray = [];
      var map;
      var objek;
	   var directionsService;
        var directionDisplay;
	  var usegeolocation;
	  var server="<?php echo Request::root(); ?>";
	   var markerarraygeo=[];
        var circlearray=[];
        var layernya;

       function initialize() {
       geolocation();
       basemap();
        }
  function basemap() {
  		google.maps.visualRefresh = true;
      map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 16,
          center: new google.maps.LatLng(-0.914813, 100.458801),
          mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      loadLayer();
  };

	function loadLayer(){
    layernya = new google.maps.Data();
    layernya.loadGeoJson(server+'/layer');
    layernya.setMap(map);

  }
        function loaddata(results){
		if(results.features === null)
         {
             alert("Data yang dicari tidak ada");
         }
         else
         {
          for (var i = 0; i < results.features.length; i++) {
              var data = results.features[i];
              var coords = data.geometry.coordinates;
              var id = data.properties.id;
              var nama = data.properties.nama;
              var alamat= data.properties.alamat;
              var deskripsi=data.properties.deskripsi;
              var titiktengah = data.properties.center
              var latitude = titiktengah.lat;
              var longitude = titiktengah.lng;
              var latLng = new google.maps.LatLng(latitude,longitude);
		  var gambar=data.properties.image; //menmpilkan informasi pada pencarian
		     $('.listdaftar').append('<p><b><a class="fancybox" href="#berita'+id+'"">'+nama+'</a></b></p><p>'+alamat+'</p><p>Travel Mode : <select id="travelmode"><option value="DRIVING">Driving</option><option value="WALKING">Walking</option><option value="BICYCLING">Bicycling</option><option value="TRANSIT">Transit</option></select></p><button id="'+i+'" class="buttongetdirection">Get Direction</button><br></div><br>');
			 //$('#berita').append('<div id="berita'+id+'" style="width:600px;display:none;text-align:justify;"><h2><b><center>'+nama+'</b></h2><br><br><center><img src="'+server+'/'+gambar+'" style="width:80%;"></center><br><br><p style="margin:5px;">'+deskripsi+'</p></div>');

          var iconBase = 'http://maps.google.com/mapfiles/kml/pal2/';
          var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            animation: google.maps.Animation.DROP,
           icon: iconBase + 'icon49.png',
            title: nama
            //shadow: iconBase + 'schools_maps.shadow.png'
          });
		  markerarray.push(marker); //menmpilkan informasi pada marking
          var isiinfo="<div style='width:200px; min-height:100px;'><b><h2><center>"+nama+"</center></h2></b><center><img src='"+server+"/"+gambar+"' style='width:80%;'></center><br><center><p>"+alamat+"</p></center></div>";
          createInfoWindow(marker, isiinfo);
         }
		}

		$('.buttongetdirection').click(function(){

			  $("#daftar").prepend('<div id="paneldirection"></div>');
			  var k=this.id;
              var titikawal=geomarker.getPosition();
              var titikakhir=markerarray[k].getPosition();
              calcRoute(titikawal,titikakhir);
              clearmarker();
			  $('.listdaftar').html('');
		      });
       }

       var infowindow = new google.maps.InfoWindow();
       function createInfoWindow(marker, isiinfo) {
          google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent(isiinfo);
          infowindow.open(map, this);
          });
       }
      google.maps.event.addDomListener(window, 'load', initialize);

	  function calcRoute(start, end) {
       var travelmode = document.getElementById("travelmode").options[document.getElementById("travelmode").selectedIndex].value;
       directionsService = new google.maps.DirectionsService();
        var request = {
         origin:start,
         destination:end,
         travelMode: google.maps.TravelMode[travelmode],
         unitSystem: google.maps.UnitSystem.METRIC,
         provideRouteAlternatives: true
       };
       directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
         directionsDisplay.setDirections(response);
        }
       });
       directionsDisplay = new google.maps.DirectionsRenderer({draggable: false});
       directionsDisplay.setMap(map);
	   directionsDisplay.setPanel(document.getElementById('paneldirection'));
	   }

	   function clearmarkergeo(){
           for (var i = 0; i < markerarraygeo.length; i++) {
            markerarraygeo[i].setMap(null);
           }
           markerarraygeo=[];
       }

       function clearmarker(){
           for (var i = 0; i < markerarray.length; i++) {
            markerarray[i].setMap(null);
           }
           markerarray=[];
       }

       function clearroute(){
           directionsDisplay.setMap(null);
       }


	   function geolocation(){
       navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError);
       }

       function geolocationSuccess(posisi){
       var pos=new google.maps.LatLng(posisi.coords.latitude,posisi.coords.longitude);
       geomarker = new google.maps.Marker({
            map: map,
            position: pos,
            animation: google.maps.Animation.DROP
	    });

        map.panTo(pos);
          infowindow = new google.maps.InfoWindow();
          infowindow.setContent('Your Position');
          infowindow.open(map, geomarker);
        usegeolocation=true;
       }

       function geolocationError(err){
           usegeolocation=false;

       }
    </script>
    <script type="text/javascript">
		$(function(){
			$('#cari').click(function(){
				$('#paneldirection').html('');
				clearmarker();
				$(".listdaftar").html("");
				var hasil = document.getElementById("selectjenis").value;
				//var jenis = document.getElementById("jenis").value;
				var script1 = document.createElement('script');
                script1.src = server+'/caripeta/'+hasil+'';
				document.getElementsByTagName('head')[0].appendChild(script1);
				clearroute();

				});
		});

		</script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3UDFwI28UYpMQQqpV7YxlSBShk-7fGCc&callback=initMap">
    </script>

<div class="sixteen columns">
   	<div class="h-border">
		<div class="searchbox">
			<span class="light"><h2>Pilih Jenis Objek:</h2></span>
			<select name="selecttipe" id="selecttipe" class="small-text" placeholder="Kata kunci" >
				<option>- Jenis -</option>
			</select>
            <select name="selectjenis" id="selectjenis" class="small-text" placeholder="Jenis">
            	<option>- Pilihan -</option>
             </select>
           <!-- <input type="text" name="nama_cari" id="nama_cari" class="small-text" placeholder="Cari Nama" required/>-->
			<button id="cari" class="btn blue" >SEARCH</button>
		</div>
	</div>
</div>
<div id="berita"></div>
<section class="latest-work row">

<div class="left">
	<div class="control-group">

		  <div class="controls">
			<div id="daftar">
            <div class="listdaftar">
            </div>
        	</div>
		 </div>
	</div>


</div>
<div class="right">
	<div class="control-group">
	 <div id="map_canvas" style=""></div>
	</div>
</div>

</section>
