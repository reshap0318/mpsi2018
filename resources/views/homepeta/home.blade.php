<?php
  include "awalan/koneksi.php";
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- CSS Saya -->
    <link rel="stylesheet" href="{{asset('awalan/style.css')}}">

    <!-- Font Viga -->
    <link href="https://fonts.googleapis.com/css?family=Viga" rel="stylesheet">

    <!-- peta unand -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKH2F9gZMQyATwBodQsEr-uM0fokVCvZw&callback=initMap"></script>

    <title>Peta Unand</title>
  </head>
  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
      <a class="navbar-brand" href="{{route('beranda')}}">PETAUNAND</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav ml-auto">

          </div>
          <script>
            // Note: This example requires that you consent to location sharing when
            // prompted by your browser. If you see the error "The Geolocation service
            // failed.", it means you probably did not give permission for the browser to
            // locate you.
            var map, infoWindow;
            function initMap() {
              map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 16
              });
              infoWindow = new google.maps.InfoWindow;

              // Try HTML5 geolocation.
              if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                  var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                  };

                  infoWindow.setPosition(pos);
                  infoWindow.setContent('lokasi sekarang');
                  infoWindow.open(map);
                  map.setCenter(pos);
                }, function() {
                  handleLocationError(true, infoWindow, map.getCenter());
                });
              } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
              }
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
              infoWindow.setPosition(pos);
              infoWindow.setContent(browserHasGeolocation ?
                                    'Error: The Geolocation service failed.' :
                                    'Error: Your browser doesn\'t support geolocation.');
              infoWindow.open(map);
            }
          </script>
          <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3UDFwI28UYpMQQqpV7YxlSBShk-7fGCc&callback=initMap">
          </script>
      </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->

    <!-- Jumbotron -->
    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <h1 class="display-4">Aplikasi <span>Peta Unand</span> Terpadu</h1>
          <a href="" class="btn btn-primary tombol">Lebih Lanjut</a>
      </div>
    </div>
    <!-- Akhir Jumbotron -->

    <!-- Container -->
    <div class="container">
      <!-- Info Panel -->
      <div class="row justify-content-center">
        <div class="col-10 info-panel">
          <div class="row">
            <div class="col-lg">
              <img src="{{asset('awalan/img/kaca-pembesar.png')}}" alt="" class="float-left">
              <h4>Pencarian</h4>
              <p>Pencarian data lengkap untuk fasilitas yang ada di Universitas Andalas</p>
            </div>
            <div class="col-lg">
              <img src="{{asset('awalan/img/security.png')}}" alt="" class="float-left">
              <h4>Informasi</h4>
              <p>Informasi lengkap terkait dengan penggunaan fasilitas yang ada di Universitas Andalas</p>
            </div>
            <div class="col-lg">
              <img src="{{asset('awalan/img/map-localization.png')}}" alt="" class="float-left">
              <h4>Navigasi</h4>
              <p>Petunjuk untuk menuju ke titik fasilitas yang ada di Universitas Andalas</p>
            </div>
          </div>
        </div>
      </div>
      <!-- Akhir info panel -->

      <!-- Konten -->
      <div class="row workingspace">
        <div class="col-lg-6 img-fluid" id="map" style="height:400px">

        </div>
        <div class="col-lg-5">
          <h3><span>Cari</span> Tempat <span>Tujuan Mu</span></h3>
          <p>Cari Lokasi / Gedung Tujuan yang Akan Kamu Tuju Selamat. Atau Jika Kamu Tersesat Bisa Gunakan Aplikasi Ini</p>
          <a href="{{route('detail')}}" class="btn btn-primary tombol">Lanjut</a>
        </div>
      </div>
      <div class="row workingspace justify-content-center">
        <h3 class="display-4">Data Fasilitas</h3>
      </div>
      <div class="row workingspace">
        <table class="table table-hover table-bordered">
          <thead>
            <tr>
              <th scope="col">NO</th>
              <th scope="col">Nama</th>
            </tr>
          </thead>
          <tbody>
            <?php $no = 0; ?>
            @foreach($fasilitas as $fas)
            <tr>
              <th scope="row">{{++$no}}</th>
              <td>{{$fas->nama}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- Akhir Konten -->

      <!-- Testimoni -->
      <section class="testimonial">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <h5>Aplikasi ini diharapkan dapat membantu mahasiswa atau pengunjung dalam menemukan fasilitas yang ada di kampus Universitas Andalas ini</h5>
          </div>
        </div>

        <div class="row justify-content-center">
          <div class="col-6 justify-content-center d-flex">
            <figure class="figure">
              <img src="{{asset('awalan/img/hafiz.jpg')}}" class="figure-img img-fluid rounded-circle" alt="testi1">
            </figure>
            <figure class="figure">
              <img src="{{asset('awalan/img/hafiz.jpg')}}" class="figure-img img-fluid rounded-circle utama" alt="testi1">
                <figcaption class="figure-caption">
                  <h5>Hafiz</h5>
                  <p>Mahasiswa</p>
                </figcaption>
            </figure>
            <figure class="figure">
              <img src="{{asset('awalan/img/hafiz.jpg')}}" class="figure-img img-fluid rounded-circle" alt="testi1">
            </figure>
          </div>
        </div>
      </section>
      <!-- Akhir Testimoni -->
    </div>
    <!-- Akhir Container -->

    <!-- Footer -->
    <div class="row">
      <div class="col text-center footer">
        <p>2018 &copy All Rights Reserved by Sistem Informasi Universitas Andalas</p>
      </div>
    </div>
    <!-- Akhir Footer -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
