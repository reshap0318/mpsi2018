
    {!! Form::hidden('id_ruangan', $id_ruangan, null, ['class' => 'form-control']) !!}

<!-- Id Fasilitas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_fasilitas', 'Id Fasilitas:') !!}
    {!! Form::select('id_fasilitas', $fasilitas, null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['0' => 'Rusak', '1' => 'Baik'], null, ['class' => 'form-control']) !!}
</div>

<!-- Foto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('foto', 'Foto:') !!}
    {!! Form::file('foto') !!}
</div>
<div class="clearfix"></div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ruanganFasilitas.index','id='.$id_ruangan) !!}" class="btn btn-default">Cancel</a>
</div>
