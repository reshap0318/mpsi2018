<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ruanganFasilitas->id !!}</p>
</div>

<!-- Id Ruangan Field -->
<div class="form-group">
    {!! Form::label('id_ruangan', 'Id Ruangan:') !!}
    <p>{!! $ruanganFasilitas->id_ruangan !!}</p>
</div>

<!-- Id Fasilitas Field -->
<div class="form-group">
    {!! Form::label('id_fasilitas', 'Id Fasilitas:') !!}
    <p>{!! $ruanganFasilitas->id_fasilitas !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ruanganFasilitas->status !!}</p>
</div>

<!-- Foto Field -->
<div class="form-group">
    {!! Form::label('foto', 'Foto:') !!}
    <p>{!! $ruanganFasilitas->foto !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ruanganFasilitas->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ruanganFasilitas->updated_at !!}</p>
</div>

