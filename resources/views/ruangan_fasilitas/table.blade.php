<table class="table table-responsive" id="ruanganFasilitas-table">
    <thead>
        <tr>
            <th>Ruangan</th>
        <th>Fasilitas</th>
        <th>Status</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>
      @if($ruanganFasilitas)
        @foreach($ruanganFasilitas as $ruanganFasilitas)
            <tr>
                <td>{!! $ruanganFasilitas->ruangans->nama !!}</td>
                <td>{!! $ruanganFasilitas->fasilitas->nama !!}</td>
                <td>
                  @if($ruanganFasilitas->status==1)
                    Baik
                  @elseif($ruanganFasilitas->status==0)
                    Rusak
                  @else
                    Tidak Diketahui
                  @endif
                </td>
                <td>
                    {!! Form::open(['route' => ['ruanganFasilitas.destroy', $ruanganFasilitas->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('ruanganFasilitas.show', [$ruanganFasilitas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('ruanganFasilitas.edit', [$ruanganFasilitas->id,'type=1']) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
      @endif
    </tbody>
</table>
