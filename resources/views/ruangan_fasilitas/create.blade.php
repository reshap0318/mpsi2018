@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ruangan Fasilitas
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ruanganFasilitas.store','files' => true,'data-parsley-validate']) !!}

                        @include('ruangan_fasilitas.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
