<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $gedung->id !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $gedung->nama !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $gedung->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $gedung->updated_at !!}</p>
</div>

<!-- Geom Field -->
<div class="form-group">
    {!! Form::label('geom', 'Map:') !!}
    <div id="map-content" style="width:100%;height:420px; z-index:50"></div>
</div>

@section('scripts')
    <script>
        var map;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map-content'),{
                center: new google.maps.LatLng(-0.915282, 100.460717),
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
                zoomControl: true,
                mapTypeControl: true
            });

            var polyOptions = {
                fillColor: 'blue',
                strokeColor: 'blue',
                draggable: true
            };
            infoWindow = new google.maps.InfoWindow;

            if (navigator.geolocation)
            {
              navigator.geolocation.getCurrentPosition(function(position)
              {
                var pos =
                {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };

                infoWindow.setPosition(pos);
                infoWindow.setContent('Lokasi Saya.');
                infoWindow.open(map);
                map.setCenter(pos);
              },
              function()
              {
                handleLocationError(true, infoWindow, map.getCenter());
              });
            } else {
              // Browser doesn't support Geolocation
              handleLocationError(false, infoWindow, map.getCenter());
            }
            getMap();
        }

        function getMap() {
            var curMap = new google.maps.Data();
            curMap.loadGeoJson('{!! route('gedunggeom',['id'=>$gedung->id]) !!}');
            curMap.setStyle(function(feature){
                return({
                    fillColor: '#bd0d2b',
                    strokeColor: '#000000',
                    strokeWeight: 1,
                    fillOpacity: 0.3
                });
            });
            curMap.setMap(map);
            $.get('{!! route('tengahgedunggeom',['id'=>$gedung->id]) !!}',function(result){
                map.setCenter(new google.maps.LatLng(result.lat,result.lon));
                map.setZoom(16);
            });
        }
    </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3UDFwI28UYpMQQqpV7YxlSBShk-7fGCc&callback=initMap"></script>

@endsection
