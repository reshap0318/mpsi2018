<table class="table table-responsive" id="gedungs-table">
    <thead>
        <tr>
            <th style="width:70%">Nama</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($gedungs as $gedung)
        <tr>
            <td>{!! $gedung->nama !!}</td>
            <td class="text-center">
                {!! Form::open(['route' => ['gedungs.destroy', $gedung->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('gedungs.show', [$gedung->id]) !!}" class='btn btn-default btn-xs'>Detail</a>
                    <a href="{!! route('gedungs.edit', [$gedung->id]) !!}" class='btn btn-default btn-xs'>Edit</a>
                    {!! Form::button('Hapus', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
