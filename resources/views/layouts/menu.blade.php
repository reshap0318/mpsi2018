<li class="{{ Request::is('gedungs*') ? 'active' : '' }}">
    <a href="{!! route('gedungs.index') !!}"><i class="fa fa-edit"></i><span>Gedung</span></a>
</li>
<li class="{{ Request::is('fasilitas*') ? 'active' : '' }}">
    <a href="{!! route('fasilitas.index') !!}"><i class="fa fa-edit"></i><span>Fasilitas</span></a>
</li>
<li class="{{ Request::is('ruangans*') ? 'active' : '' }}">
    <a href="{!! route('ruangans.index') !!}"><i class="fa fa-edit"></i><span>Ruangan</span></a>
</li>
