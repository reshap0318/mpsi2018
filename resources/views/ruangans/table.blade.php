<table class="table table-responsive" id="ruangans-table">
    <thead>
        <tr>
            <th  style="width:70%">Nama</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ruangans as $ruangan)
        <tr>
            <td>{!! $ruangan->nama !!}</td>
            <td class="text-center">
                {!! Form::open(['route' => ['ruangans.destroy', $ruangan->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ruangans.show', [$ruangan->id]) !!}" class='btn btn-success btn-xs'>detail</a>
                    <a href="{!! route('ruangans.edit', [$ruangan->id]) !!}" class='btn btn-info btn-xs'>Edit</a>
                    <a href="{!! route('ruanganFasilitas.index', 'id='.$ruangan->id) !!}" class='btn btn-warning btn-xs'>Fasilitas Ruangan</a>
                    {!! Form::button('Hapus', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
