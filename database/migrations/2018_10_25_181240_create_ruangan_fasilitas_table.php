<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateruanganFasilitasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ruangan_fasilitas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_ruangan')->unsigned();
            $table->integer('id_fasilitas')->unsigned();
            $table->integer('status');
            $table->string('foto');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_ruangan')->references('id')->on('ruangans');
            $table->foreign('id_fasilitas')->references('id')->on('fasilitas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ruangan_fasilitas');
    }
}
