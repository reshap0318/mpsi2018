<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreategedungRequest;
use App\Http\Requests\UpdategedungRequest;
use App\Repositories\gedungRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\gedung;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class gedungController extends AppBaseController
{
    /** @var  gedungRepository */
    private $gedungRepository;

    public function __construct(gedungRepository $gedungRepo)
    {
        $this->gedungRepository = $gedungRepo;
    }

    /**
     * Display a listing of the gedung.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->gedungRepository->pushCriteria(new RequestCriteria($request));
        $gedungs = gedung::orderby('nama','asc')->get();

        return view('gedungs.index')
            ->with('gedungs', $gedungs);
    }

    /**
     * Show the form for creating a new gedung.
     *
     * @return Response
     */
    public function create()
    {
        return view('gedungs.create');
    }

    /**
     * Store a newly created gedung in storage.
     *
     * @param CreategedungRequest $request
     *
     * @return Response
     */
    public function store(CreategedungRequest $request)
    {
        $input = $request->all();
        $input['geom'] = "MULTIPOLYGON(".$input['geom'].")";

        $gedung = $this->gedungRepository->create($input);

        Flash::success('Gedung saved successfully.');

        return redirect(route('gedungs.index'));
    }

    /**
     * Display the specified gedung.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gedung = $this->gedungRepository->findWithoutFail($id);

        if (empty($gedung)) {
            Flash::error('Gedung not found');

            return redirect(route('gedungs.index'));
        }

        return view('gedungs.show')->with('gedung', $gedung);
    }

    /**
     * Show the form for editing the specified gedung.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $gedung = $this->gedungRepository->findWithoutFail($id);

        if (empty($gedung)) {
            Flash::error('Gedung not found');

            return redirect(route('gedungs.index'));
        }

        return view('gedungs.edit')->with('gedung', $gedung);
    }

    /**
     * Update the specified gedung in storage.
     *
     * @param  int              $id
     * @param UpdategedungRequest $request
     *
     * @return Response
     */
    public function update($id, UpdategedungRequest $request)
    {
        $gedung = $this->gedungRepository->findWithoutFail($id);

        if (empty($gedung)) {
            Flash::error('Gedung not found');

            return redirect(route('gedungs.index'));
        }
        $input = $request->all();
        $input['geom'] = "MULTIPOLYGON(".$input['geom'].")";
        $gedung = $this->gedungRepository->update($input, $id);

        Flash::success('Gedung updated successfully.');

        return redirect(route('gedungs.index'));
    }

    /**
     * Remove the specified gedung from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gedung = $this->gedungRepository->findWithoutFail($id);

        if (empty($gedung)) {
            Flash::error('Gedung not found');

            return redirect(route('gedungs.index'));
        }

        $this->gedungRepository->delete($id);

        Flash::success('Gedung deleted successfully.');

        return redirect(route('gedungs.index'));
    }

    public function getMapgedung($id){
        if ($id != 'all'){
            $store = gedung::where('id',$id)->get();
        }else{
            $store = gedung::all();
        }
        $ret = config('value.t1');
        $i = 0;
        foreach ($store as $l){
            $ret['features'][$i] = config('value.t2');
            $ret['features'][$i]['geometry'] = $l->geom;
            $i++;
        }
        return response()->json($ret);
    }

    public function getCentergedung($id){
        $store = gedung::select(DB::raw("ST_X(ST_Centroid(geom)) AS lon, ST_Y(ST_CENTROID(geom)) As lat"))
            ->where('id',$id)->first();
        return response()->json($store);
    }


}
