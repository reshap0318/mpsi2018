<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Input;
use App\Models\gedung;
use App\Models\ruangan;
use App\Models\fasilitas;
use DB;


class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         return view('home');
     }

     public function beranda()
     {
        $fasilitas = fasilitas::all();
         return view('homepeta.home',compact('fasilitas'));
     }

     public function detail()
     {

       $gedungs = gedung::orderby('nama','asc')->get();
       return view('homepeta.detail',compact('gedungs'));
     }

     public function ruangan()
     {
       $gedungid = input::get('gedungid');
       $ruangan = ruangan::whereRaw('st_contains((select geom from gedungs where id = '.$gedungid.'), geom)')->get();
        return response()->json($ruangan);
     }




     public function layerpeta()
     {
       $layer = DB::SELECT(DB::RAW('select ST_asgeojson(geom) AS geometry, id,nama FROM ruangans'));
       $hasil = array(
        	'type'	=> 'FeatureCollection',
        	'features' => array()
      	);

        foreach ($layer as $isinya) {
          $features = array(
            'type' => 'Feature',
            'geometry' => json_decode($isinya->geometry),
            'properties' => array(
              'id' => $isinya->id,
              'nama' => $isinya->nama,
              )
            );
          array_push($hasil['features'], $features);
        }
        return response()->json($hasil);
     }

     public function caripeta($id)
     {
       $caripeta = DB::SELECT(DB::RAW("SELECT row_to_json(fc) FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features FROM (SELECT 'Feature' As type , ST_AsGeoJSON(loc.geom)::json As geometry , row_to_json((SELECT l FROM (SELECT id,nama, row_to_json((SELECT k FROM (SELECT ST_X(ST_CENTROID(geom)) AS lng, ST_Y(ST_CENTROID(geom)) AS lat) AS k)) AS center) As l )) As properties FROM ruangans As loc WHERE id=$id) As f ) As fc"));

       foreach ($caripeta as $data) {
         $load=$data->row_to_json;
         $tulis="loaddata(".$load.");";
       }
      	return $tulis;
     }

     public function caritype()
     {
       $caritype = DB::SELECT(DB::RAW("select id,nama from gedungs"));
       return response()->json($caritype);
     }

}
