<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateruanganFasilitasRequest;
use App\Http\Requests\UpdateruanganFasilitasRequest;
use App\Repositories\ruanganFasilitasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\fasilitas;
use App\Models\ruangan;
use App\Models\ruanganFasilitas;
use Illuminate\Support\Facades\File;

class ruanganFasilitasController extends AppBaseController
{
    /** @var  ruanganFasilitasRepository */
    private $ruanganFasilitasRepository;

    public function __construct(ruanganFasilitasRepository $ruanganFasilitasRepo)
    {
        $this->ruanganFasilitasRepository = $ruanganFasilitasRepo;
    }

    /**
     * Display a listing of the ruanganFasilitas.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ruanganFasilitasRepository->pushCriteria(new RequestCriteria($request));
        $ruanganFasilitas = $this->ruanganFasilitasRepository->all();
        $id = $request->id;
        if($id){
          $ruanganFasilitas = ruanganFasilitas::where('id_ruangan',$id)->get();
        }
        $ruangan = ruangan::find($id);
        return view('ruangan_fasilitas.index',compact('ruanganFasilitas','ruangan'));
    }

    /**
     * Show the form for creating a new ruanganFasilitas.
     *
     * @return Response
     */
    public function create(Request $request)
    {
      $id_ruangan = $request->id;
      //dd($id_ruangan);
      if(!$id_ruangan){
        return redirect()->back();
      }
        $fasilitas = fasilitas::pluck('nama','id');
        return view('ruangan_fasilitas.create',compact('fasilitas','id_ruangan'));
    }

    /**
     * Store a newly created ruanganFasilitas in storage.
     *
     * @param CreateruanganFasilitasRequest $request
     *
     * @return Response
     */
    public function store(CreateruanganFasilitasRequest $request)
    {
      $request->validate([
        'id_ruangan' => 'required',
        'id_fasilitas' => 'required',
        'foto' => 'image|mimes:jpg,png,jpeg,gif',
      ]);
        $input = $request->all();
        if ($request->hasFile('foto') && $request->foto->isValid()) {
            $path = 'img/foto';
            $oldfile = $input['foto'];

            $fileext = $request->foto->extension();
            $filename = uniqid("fotos-").'.'.$fileext;

            //Real File
            $filepath = $request->file('foto')->storeAs($path, $filename, 'local');
            //foto File
            $realpath = storage_path('app/'.$filepath);
            $input['foto'] = $filename;
            //hapus foto lama
            File::delete(storage_path('app'.'/'. $path . '/' . $oldfile));
            File::delete(public_path($path . '/' . $oldfile));
        }
        $ruanganFasilitas = $this->ruanganFasilitasRepository->create($input);

        Flash::success('Ruangan Fasilitas saved successfully.');

        return redirect(route('ruanganFasilitas.index','id='.$ruanganFasilitas->id_ruangan));
    }

    /**
     * Display the specified ruanganFasilitas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ruanganFasilitas = $this->ruanganFasilitasRepository->findWithoutFail($id);

        if (empty($ruanganFasilitas)) {
            Flash::error('Ruangan Fasilitas not found');

            return redirect(route('ruanganFasilitas.index','id='.$ruanganFasilitas->id_ruangan));
        }

        return view('ruangan_fasilitas.show')->with('ruanganFasilitas', $ruanganFasilitas);
    }

    /**
     * Show the form for editing the specified ruanganFasilitas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id, Request $request)
    {
      $id_ruangan = $request->type;
      //dd($id_ruangan);
      if(!$id_ruangan){
        return redirect()->back();
      }
        $ruanganFasilitas = $this->ruanganFasilitasRepository->findWithoutFail($id);
        $fasilitas = fasilitas::pluck('nama','id');
        if (empty($ruanganFasilitas)) {
            Flash::error('Ruangan Fasilitas not found');

            return redirect(route('ruanganFasilitas.index','id='.$ruanganFasilitas->id_ruangan));
        }

        return view('ruangan_fasilitas.edit',compact('fasilitas','id_ruangan','ruanganFasilitas'));
    }

    /**
     * Update the specified ruanganFasilitas in storage.
     *
     * @param  int              $id
     * @param UpdateruanganFasilitasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateruanganFasilitasRequest $request)
    {
        $ruanganFasilitas = $this->ruanganFasilitasRepository->findWithoutFail($id);

        if (empty($ruanganFasilitas)) {
            Flash::error('Ruangan Fasilitas not found');

            return redirect(route('ruanganFasilitas.index','id='.$ruanganFasilitas->id_ruangan));
        }

        $ruanganFasilitas = $this->ruanganFasilitasRepository->update($request->all(), $id);

        Flash::success('Ruangan Fasilitas updated successfully.');

        return redirect(route('ruanganFasilitas.index','id='.$ruanganFasilitas->id_ruangan));
    }

    /**
     * Remove the specified ruanganFasilitas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ruanganFasilitas = $this->ruanganFasilitasRepository->findWithoutFail($id);

        if (empty($ruanganFasilitas)) {
            Flash::error('Ruangan Fasilitas not found');

            return redirect(route('ruanganFasilitas.index','id='.$ruanganFasilitas->id_ruangan));
        }

        $this->ruanganFasilitasRepository->delete($id);

        Flash::success('Ruangan Fasilitas deleted successfully.');

        return redirect(route('ruanganFasilitas.index','id='.$ruanganFasilitas->id_ruangan));
    }
}
