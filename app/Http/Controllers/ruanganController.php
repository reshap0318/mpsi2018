<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateruanganRequest;
use App\Http\Requests\UpdateruanganRequest;
use App\Repositories\ruanganRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\ruangan;

class ruanganController extends AppBaseController
{
    /** @var  ruanganRepository */
    private $ruanganRepository;

    public function __construct(ruanganRepository $ruanganRepo)
    {
        $this->ruanganRepository = $ruanganRepo;
    }

    /**
     * Display a listing of the ruangan.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ruanganRepository->pushCriteria(new RequestCriteria($request));
        $ruangans = $this->ruanganRepository->all();

        return view('ruangans.index')
            ->with('ruangans', $ruangans);
    }

    /**
     * Show the form for creating a new ruangan.
     *
     * @return Response
     */
    public function create()
    {
        return view('ruangans.create');
    }

    /**
     * Store a newly created ruangan in storage.
     *
     * @param CreateruanganRequest $request
     *
     * @return Response
     */
    public function store(CreateruanganRequest $request)
    {
        $input = $request->all();
        $input['geom'] = "MULTIPOLYGON(".$input['geom'].")";
        $ruangan = $this->ruanganRepository->create($input);

        Flash::success('Ruangan saved successfully.');

        return redirect(route('ruangans.index'));
    }

    /**
     * Display the specified ruangan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ruangan = $this->ruanganRepository->findWithoutFail($id);

        if (empty($ruangan)) {
            Flash::error('Ruangan not found');

            return redirect(route('ruangans.index'));
        }

        return view('ruangans.show')->with('ruangan', $ruangan);
    }

    /**
     * Show the form for editing the specified ruangan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ruangan = $this->ruanganRepository->findWithoutFail($id);

        if (empty($ruangan)) {
            Flash::error('Ruangan not found');

            return redirect(route('ruangans.index'));
        }

        return view('ruangans.edit')->with('ruangan', $ruangan);
    }

    /**
     * Update the specified ruangan in storage.
     *
     * @param  int              $id
     * @param UpdateruanganRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateruanganRequest $request)
    {
        $ruangan = $this->ruanganRepository->findWithoutFail($id);
        $input = $request->all();
        $input['geom'] = "MULTIPOLYGON(".$input['geom'].")";
        if (empty($ruangan)) {
            Flash::error('Ruangan not found');

            return redirect(route('ruangans.index'));
        }

        $ruangan = $this->ruanganRepository->update($input, $id);

        Flash::success('Ruangan updated successfully.');

        return redirect(route('ruangans.index'));
    }

    /**
     * Remove the specified ruangan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ruangan = $this->ruanganRepository->findWithoutFail($id);

        if (empty($ruangan)) {
            Flash::error('Ruangan not found');

            return redirect(route('ruangans.index'));
        }

        $this->ruanganRepository->delete($id);

        Flash::success('Ruangan deleted successfully.');

        return redirect(route('ruangans.index'));
    }

    public function getMapruangan($id){
        if ($id != 'all'){
            $store = ruangan::where('id',$id)->get();
        }else{
            $store = ruangan::all();
        }
        $ret = config('value.t1');
        $i = 0;
        foreach ($store as $l){
            $ret['features'][$i] = config('value.t2');
            $ret['features'][$i]['geometry'] = $l->geom;
            $i++;
        }
        return response()->json($ret);
    }

    public function getCentergedung($id){
        $store = ruangan::select(DB::raw("ST_X(ST_Centroid(geom)) AS lon, ST_Y(ST_CENTROID(geom)) As lat"))
            ->where('id',$id)->first();
        return response()->json($store);
    }
}
