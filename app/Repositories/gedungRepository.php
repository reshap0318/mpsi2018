<?php

namespace App\Repositories;

use App\Models\gedung;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class gedungRepository
 * @package App\Repositories
 * @version October 25, 2018, 5:55 pm UTC
 *
 * @method gedung findWithoutFail($id, $columns = ['*'])
 * @method gedung find($id, $columns = ['*'])
 * @method gedung first($columns = ['*'])
*/
class gedungRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return gedung::class;
    }
}
