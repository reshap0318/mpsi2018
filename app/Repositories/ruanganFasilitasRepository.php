<?php

namespace App\Repositories;

use App\Models\ruanganFasilitas;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ruanganFasilitasRepository
 * @package App\Repositories
 * @version October 25, 2018, 6:12 pm UTC
 *
 * @method ruanganFasilitas findWithoutFail($id, $columns = ['*'])
 * @method ruanganFasilitas find($id, $columns = ['*'])
 * @method ruanganFasilitas first($columns = ['*'])
*/
class ruanganFasilitasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_ruangan',
        'id_fasilitas',
        'status',
        'foto'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ruanganFasilitas::class;
    }
}
