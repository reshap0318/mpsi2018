<?php

namespace App\Repositories;

use App\Models\ruangan;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ruanganRepository
 * @package App\Repositories
 * @version October 25, 2018, 5:57 pm UTC
 *
 * @method ruangan findWithoutFail($id, $columns = ['*'])
 * @method ruangan find($id, $columns = ['*'])
 * @method ruangan first($columns = ['*'])
*/
class ruanganRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ruangan::class;
    }
}
