<?php

namespace App\Repositories;

use App\Models\fasilitas;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class fasilitasRepository
 * @package App\Repositories
 * @version October 25, 2018, 6:03 pm UTC
 *
 * @method fasilitas findWithoutFail($id, $columns = ['*'])
 * @method fasilitas find($id, $columns = ['*'])
 * @method fasilitas first($columns = ['*'])
*/
class fasilitasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return fasilitas::class;
    }
}
