<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ruanganFasilitas
 * @package App\Models
 * @version October 25, 2018, 6:12 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection id
 * @property \App\Models\fasilitas fasilitas
 * @property integer id_ruangan
 * @property integer id_fasilitas
 * @property integer status
 * @property string foto
 */
class ruanganFasilitas extends Model
{
    public $table = 'ruangan_fasilitas';


    public $fillable = [
        'id_ruangan',
        'id_fasilitas',
        'status',
        'foto'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_ruangan' => 'integer',
        'id_fasilitas' => 'integer',
        'status' => 'integer',
        'foto' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_ruangan' => 'required',
        'id_fasilitas' => 'required',
        'status' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function ruangans()
    {
        return $this->belongsTo(\App\Models\ruangan::class, 'id_ruangan', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function fasilitas()
    {
        return $this->belongsTo(\App\Models\fasilitas::class, 'id_fasilitas', 'id');
    }
}
