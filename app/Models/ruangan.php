<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;

/**
 * Class ruangan
 * @package App\Models
 * @version October 25, 2018, 5:57 pm UTC
 *
 * @property string nama
 */
class ruangan extends Model
{
    use PostgisTrait;

    public $table = 'ruangans';


    public $fillable = [
        'nama', 'geom'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required'
    ];

    protected $postgisFields = [
        'geom'
    ];

    protected $postgisTypes = [
        'geom' => [
            'geomtype' => 'geometry',
            'srid' => 0
        ]
    ];


}
